<?php 

require 'functions.php';


try {
	$pdo = new PDO('mysql:host=127.0.0.1;dbname=mytodo', 'root', '');
} catch (PDOException $e) {
	die($e->getMessage());
	// die('Could not Connect');
}

$statement =  $pdo->prepare('select * from todos');

$statement->execute();

$todos = $statement->fetchAll(PDO::FETCH_OBJ);

$names = [
	'Sandeep',
	'Vj',
	'Surf'
];

// foreach ($names as $name) {
// 	echo '<li>'.$name.'</li>';
// }

$task = [
	'title' => 'Finish HomeWork',
	'due' => 'total',
	'assigned_to' => 'Sandeep',
	'completed' => true,
	'age' => 18
];

// ageLimit($task['age']);


class Task
{
	public $description;
	public $completed = false;
	public function __construct ($description)
	{
		$this->description = $description;
	}
	public function iscompleted ()
	{
		return $this->completed;
	}
	public function complete ()
	{
		$this->completed = true; 
	}
};

$tasks = [
	new Task ('Complete all To Dos'),
	new Task ("Go to sleep early"),
	new Task ("finish your work")
];

$tasks[1]->complete();

// dd($tasks[1]->iscompleted());

require 'index.view.php';

// dd($task);

?>