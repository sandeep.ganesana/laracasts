<html>
	<head>
		<meta charset="UTF-8">
		<title>Practise</title>
		<style>
			header {
				background: #e3e3e3;
				padding: 5px;
				text-align: center;
			}
		</style>
	</head>
	<body>
		<ul>
			<?php foreach ($todos as $task) : ?>
				<?php if ($task->completed) : ?>
					<li><strike><?= $task->description; ?></strike></li>
				<?php else : ?>
					<li><?= $task->description; ?></li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
	</body>
</html>